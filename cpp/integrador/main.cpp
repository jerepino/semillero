#include <iostream>
#include <list>
#include <map>
#include <string>

typedef enum { TEMPERATURE, HUMIDITY, PRESSURE } sensor_type;

class ISensor {
   private:
    double data_;

   public:
    ISensor() { data_ = 0; };
    ISensor(double data) : data_(data){};
    ~ISensor() = default;
    virtual void setData(double data) { this->data_ = data; }
    double getData() { return this->data_; }
};

class ISubscriber {
   public:
    virtual ~ISubscriber(){};
    virtual void updateData(const std::map<sensor_type, ISensor> &data_from_publisher) = 0;
};

class IPublisher {
   public:
    virtual ~IPublisher(){};
    virtual void attach(ISubscriber *subscriber) = 0;
    virtual void detach(ISubscriber *subscriber) = 0;
    virtual void notify() = 0;
};

class DataCenter : public IPublisher {
   private:
    std::list<ISubscriber *> list_subscriber_;
    std::map<sensor_type, ISensor> list_sensors_;

   public:
    DataCenter(std::map<sensor_type, ISensor> list_sensors) : list_sensors_(list_sensors) {}
    ~DataCenter() { std::cout << "I'm dead\n"; }
    void attach(ISubscriber *subscriber) override { list_subscriber_.push_back(subscriber); }
    void detach(ISubscriber *subscriber) override { list_subscriber_.remove(subscriber); }
    void notify() override {
        for (auto *x : list_subscriber_) {
            x->updateData(list_sensors_);
        }
    }
    void updateList(std::map<sensor_type, ISensor> list_sensors) {
        this->list_sensors_ = list_sensors;
        notify();
    }
};

class DataViewer_1 : public ISubscriber {
   public:
    DataViewer_1(DataCenter &publisher) : publisher_(publisher) { this->publisher_.attach(this); }
    void updateData(const std::map<sensor_type, ISensor> &data_from_publisher) {
        list_sensors_ = data_from_publisher;
        printInfo();
    }
    void printInfo() {
        std::cout << "DataViewer 1. \n";
        for (auto &x : list_sensors_) {
            switch (x.first) {
                case TEMPERATURE:
                    std::cout << "La temperatura: " << x.second.getData() << "C ";
                    break;
                case HUMIDITY:
                    std::cout << "La humedad: " << x.second.getData() << "% ";
                    break;
                case PRESSURE:
                    std::cout << "La presión: " << x.second.getData() << "KPa";
                    break;
                default:
                    break;
            }
        }
        std::cout << std::endl;
    }
    void removeFromList() {
        publisher_.detach(this);
        std::cout << "Remove from the list \n";
    }

   private:
    DataCenter &publisher_;
    std::map<sensor_type, ISensor> list_sensors_;
};

class DataViewer_2 : public ISubscriber {
   public:
    DataViewer_2(DataCenter &publisher) : publisher_(publisher) { this->publisher_.attach(this); }
    void updateData(const std::map<sensor_type, ISensor> &data_from_publisher) {
        list_sensors_ = data_from_publisher;
        printInfo();
    }
    void printInfo() {
        std::cout << "DataViewer 2. \n";
        for (auto &x : list_sensors_) {
            double value = x.second.getData();
            double aver = value - value * 0.1;
            double max = value + 2;
            double min = value - 2;
            switch (x.first) {
                case TEMPERATURE:
                    std::cout << "Estadisticas de temperatura Avg/Max/Min: \n";
                    std::cout << aver << "C /" << max << "C /" << min << "C \n";
                    break;
                case HUMIDITY:
                    std::cout << "Estadisticas de humedad Avg/Max/Min: \n";
                    std::cout << aver << "% /" << max << "% /" << min << "% \n";
                    break;
                case PRESSURE:
                    std::cout << "Estadisticas de presion Avg/Max/Min: \n";
                    std::cout << aver << "KPa /" << max << "KPa /" << min << "KPa \n";
                    break;
                default:
                    break;
            }
        }
    }
    void removeFromList() {
        publisher_.detach(this);
        std::cout << "Remove from the list \n";
    }

   private:
    DataCenter &publisher_;
    std::map<sensor_type, ISensor> list_sensors_;
};

class DataViewer_3 : public ISubscriber {
   public:
    DataViewer_3(DataCenter &publisher) : publisher_(publisher) { this->publisher_.attach(this); }
    void updateData(const std::map<sensor_type, ISensor> &data_from_publisher) {
        list_sensors_ = data_from_publisher;
        printInfo();
    }
    void printInfo() {
        std::cout << "DataViewer 3. \n";
        for (auto &x : list_sensors_) {
            double value = x.second.getData();
            double aver = value - value * 0.1;
            double max = value + 2;
            double min = value - 2;
            switch (x.first) {
                case TEMPERATURE:
                    if (value > -10 && value <= 10) std::cout << "Sera mejor quedarse en casa \n";
                    if (value > 10 && value <= 20) std::cout << "Tendras que usar campera \n";
                    if (value > 20 && value <= 30) std::cout << "Va a ser un hermoso dia \n";
                    if (value > 30 && value <= 40) std::cout << "Peligro de incineración \n";
                    break;
                case HUMIDITY:
                    if (value > 0 && value <= 25) std::cout << "Estilo Sahara \n";
                    if (value > 25 && value <= 50) std::cout << "Agradable \n";
                    if (value > 50 && value <= 75) std::cout << "Probabilidad de lluvia \n";
                    if (value > 75 && value <= 100) std::cout << "Hoy no hace falta bañarse, ducha asegurada\n";
                    break;
                case PRESSURE:
                    std::cout << "Ni idea como influye la presion" << std::endl;
                    break;
                default:
                    break;
            }
        }
    }
    void removeFromList() {
        publisher_.detach(this);
        std::cout << "Remove from the list \n";
    }

   private:
    DataCenter &publisher_;
    std::map<sensor_type, ISensor> list_sensors_;
};

int main() {
    ISensor temperature;
    ISensor humidity;
    ISensor pressure;
    std::map<sensor_type, ISensor> sensors{{TEMPERATURE, temperature}, {HUMIDITY, humidity}, {PRESSURE, pressure}};
    DataCenter *weather_ptr = new DataCenter(sensors);
    ISubscriber *data_viewer_1_ptr = new DataViewer_1(*weather_ptr);
    ISubscriber *data_viewer_2_ptr = new DataViewer_2(*weather_ptr);
    ISubscriber *data_viewer_3_ptr = new DataViewer_3(*weather_ptr);

    double temp, hum, press;
    bool flag;

    while (1) {
        std::cout << "Ingrese (T[C] H[%] P[KPa]): ";
        std::cin >> temp >> hum >> press;
        for (auto &x : sensors) {
            switch (x.first) {
                case TEMPERATURE:
                    x.second.setData(temp);
                    break;
                case HUMIDITY:
                    x.second.setData(hum);
                    break;
                case PRESSURE:
                    x.second.setData(press);
                    break;
                default:
                    break;
            }
        }
        weather_ptr->updateList(sensors);
        std::cout << std::endl;
        std::cout << "Para continuar ingrese 1 para salir 0: ";
        std::cin >> flag;
        if (!flag) break;
    }

    delete data_viewer_1_ptr;
    delete data_viewer_2_ptr;
    delete data_viewer_3_ptr;
    delete weather_ptr;
}
