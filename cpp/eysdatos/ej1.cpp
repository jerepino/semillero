/*
 *
 *
 *
 *
 *
 * **/


#include <iostream>
#include <string>
#include <map>
#include <vector>


constexpr int SIZE = 9;
constexpr int MAX_ROUND = 6;


using namespace std;

void print_tablero(map<int,string> &table){
        cout<<"Tablero\n";
    for(auto &x: table){
        cout<<x.first<<" "<<x.second<<"\t";
        if(x.first%3 == 0) cout<<endl;
    }
        /* cout<<"Tablero\n"; */
        /* cout<<endl; */
        /* cout<<"7 \t8 \t9 \n"; */
        /* cout<<endl; */
        /* cout<<"4 \t5 \t6 \n"; */
        /* cout<<endl; */
        /* cout<<"1 \t2 \t3 \n"; */
        /* cout<<endl; */
}

string check_winner(const map<int,string> &table){
    int x_row = 0, o_row = 0;
    int x_cross = 0, o_cross = 0;
    int x_colum = 0, o_colum = 0;
    string winner = "Nadie";
    bool row_match = false;
    bool colum_match = false;
    bool cross_match = false;

    for(int i = 0; i < 3; i+=3){
        for(int j=0; j<3; j++){
            auto it = table.begin();
            std::advance(it, i+j);
            if(it->second == "x") x_row++;
            if(it->second == "o") o_row++;
            if(x_row == 3 || o_row ==3){
                row_match = true;
                break;
            }
        }
        x_row =0;
        o_row =0;
        if(row_match) break;
    }

    if(row_match){
        if(x_row>o_row){
            winner = "x";
        }else
        {
            winner = "o";
        }
    }

    for(int i = 0; i<3 ; i++){
        for(int j=0; j<9; j+=3){
            auto it = table.begin();
            std::advance(it, i+j);
            if(it->second == "x") x_colum++;
            if(it->second == "o") o_colum++;
            if(x_colum == 3 || o_colum == 3){
                colum_match = true;
                break;
            }
        }
        x_colum =0;
        o_colum =0;
        if(colum_match) break;
    }

    if(colum_match){
        if(x_colum > o_colum){
            winner = "x";
        }else
        {
            winner = "o";
        }
    }

    for(int i = 0; i < 9; i+=4){
            auto it = table.begin();
            std::advance(it, i);
            if(it->second == "x") x_cross++;
            if(it->second == "o") o_cross++;
            if(x_cross == 3 || o_cross ==3){
                cross_match = true;
            }
    }
    if(!cross_match){
        x_cross = 0;
        o_cross = 0;
        for(int i = 2; i < 7; i+=2){
                auto it = table.begin();
                std::advance(it, i);
                /* cout<<i<<it->second; */
                if(it->second == "x") x_cross++;
                if(it->second == "o") o_cross++;
                if(x_cross == 3 || o_cross ==3){
                    cross_match = true;
                }
        }
    }

    if(cross_match){
        if(x_cross > o_cross){
            winner = "x";
        }else
        {
            winner = "o";
        }
    }
    return winner;
}

int main(void){


    /* vector<vector<int>> matrix; */
    /* vector<int> aux; */
    /* for(int i=0;i<3; i++) aux.push_back(0); */
    /* for(int i=0; i<3; i++) matrix.push_back(aux); */
    /* for(auto &x: matrix) */
    /*     for(auto &y:x) */
    /*         cout<<y<<" "<<endl; */

    int casilla =0, p1 =0;
    string name_o, name_x, winner;
    bool flag = true;
    map<int,string> maps;

    for(int i=1;i<=SIZE; i++) maps.insert(pair<int,string>(i," "));

    cout<<"Tic-Tac-Toe\n";
    cout<<"Ingrese nombre jugador o:\n";
    getline(cin, name_o);
    cout<<"Ingrese nombre jugador x:\n";
    getline(cin, name_x);

    print_tablero(maps);
    while(flag){
        if(p1 % 2 == 0){
            cout<<"Turno de "<<name_o<<endl;
        }else{
            cout<<"Turno de "<<name_x<<endl;
        }
        cout<<"Seleccione numero de casillero: \n";
        cin>>casilla;
        if(p1 % 2 == 0){
            maps[casilla] = "o";
        }else{
            maps[casilla] = "x";
        }
        print_tablero(maps);
        winner = check_winner(maps);
        if(winner == "o" || winner == "x") break;

        p1++;
        if(casilla == 0 || p1 == MAX_ROUND ) break;
    }
    cout<<"El ganador es: "<<winner<<endl;
    return 0;
}
