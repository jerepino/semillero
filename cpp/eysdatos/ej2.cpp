#include <iostream>
#include <string>
#include <vector>

int main(){

    std::string word;
    std::string rword;
    

    std::cout<<"Input: ";
    std::cin >> word;

    rword = std::string(word.rbegin(),word.rend()-1);
    int count = -1;
    int invcount = word.length()-1;
    std::string space = " ";

    for(auto &it : word){
        for(int i = 0; i<invcount; i++) std::cout<< " ";
        if(count < 0){
            std::cout<< word.at(0)<<std::endl; 
        }
        else{
            std::cout<< it; 
            for(int i= 0; i<count; i++) std::cout<<" ";
            std::cout<< it << std::endl;
        }
        count+=2;
        invcount--;
    }
    std::cout << rword + word <<std::endl;
}