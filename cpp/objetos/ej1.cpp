#include <iostream>
#include <vector>
#include <string>

class Empleado{
    public:
        Empleado() = default;
        ~Empleado() = default;
        void ingresarDatos(){
            std::cout<<"Ingrese nombre: ";
            std::cin >> this->name;
            std::cout<<"Ingrese ID: ";
            std::cin >> this->id;
        }

        void mostratDatos(){
            std::cout<<"El nombre es: ";
            std::cout << this->name <<std::endl;
            std::cout<<"El ID es: ";
            std::cout << this->id <<std::endl;
        }
    private:
        std::string name;
        int id;
};

int main(){
    std::vector<Empleado> empleados(5);

    for(int i=0; i<5; i++){
        empleados[i].ingresarDatos();
    }
    std::cout<<"Datos de los empleados \n";
    for(int i=0; i<5; i++){
        empleados[i].mostratDatos();
    }
}