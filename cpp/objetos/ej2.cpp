#include <iostream>
#include <cmath>

class Punto {
    public:
        Punto() = default;
        Punto(double x_, double y_) : x(x_), y(y_), dist(0){};
        ~Punto() = default;
        void dist2origin()
        {
            this->dist = sqrt(pow(this->x, 2) + pow(this->y, 2));
        }
        void setX(double x_) { this->x = x_; }
        void setY(double y_) { this->y = y_; }
        double getX() { return this->x; }
        double getY() { return this->y; }
        double getDistancia() { return this->dist; }

    private:
        double x;
        double y;
        double dist;
};

int main() {
    Punto punto(10, 10);
    Punto p;
    punto.dist2origin();
    std::cout << punto.getDistancia() << std::endl;
    p.setX(10);
    p.setY(10);
    p.dist2origin();
    std::cout << p.getDistancia() << std::endl;
}
