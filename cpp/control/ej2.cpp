#include <iostream>
#include <string>
#include <math.h>

int main() {
    int dec = 0;
    std::string input;
    std::cout << "HEX: ";
    std::cin >> input;
    auto it = input.find('x');
    int n = input.length() - it;

    for (int i = it + 1; i <= n; i++) {
        switch (input[i]) {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            dec += std::stoi(&input[i]) * pow(16, (n - i));
            break;
        case 'a':
            dec += 10 * pow(16, (n - i));
            break;
        case 'b':
            dec += 11 * pow(16, (n - i));
            break;
        case 'c':
            dec += 12 * pow(16, (n - i));
            break;
        case 'd':
            dec += 13 * pow(16, (n - i));
            break;
        case 'e':
            dec += 14 * pow(16, (n - i));
            break;
        case 'f':
            dec += 15 * pow(16, (n - i));
            break;
        default:
            break;
        }
    }
    std::cout << "DEC: " << dec << std::endl;
}