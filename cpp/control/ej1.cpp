#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> words;
    std::string word;

    int n;
    std::cout << "Numero de palabras N: ";
    std::cin >> n;
    for (int i = 0; i < n; i++) {
        std::cout << "Ingrese palabra: ";
        std::cin >> word;
        words.push_back(word);
        std::cout << std::endl;
    }

    std::sort(words.begin(), words.end());

    std::cout << "Palabras ordenadas alfabeticamente: \n";
    for (auto &x : words) {
        std::cout << x << std::endl;
    }
}
