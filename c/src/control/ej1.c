/**
 * Módulo Programación en C
 * Instrucciones de control
 * Ejercicio 1
 * Autor: Jeremías
 * **/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define LONG 80

void tools_invert_string(char *out, char *in)
{
    int i, j;
    int length = strlen(in);
    for (i = 0, j = length-1; i < length; i++, j--)
    {
        out[i] = in[j];
    }
}

int main(){

    char texto[LONG];
    char invertido[LONG];
    bool flag_ = false;
    while(!flag_){
        printf("Ingrese texto o -1 para terminar \r\n");
        fgets(texto, LONG , stdin);

        if(!strncmp(texto,"-1",2)) break;

        tools_invert_string(invertido, texto);
        printf("%s\n",invertido);

        }
    return 0;
}
