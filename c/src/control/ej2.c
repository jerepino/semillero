/**
 * Módulo Programación en C
 * Instrucciones de control
 * Ejercicio 2
 * Autor: Jeremías
 * **/


#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>


double my_pow(double a, int e){
    double result = 1;
    for(int i=e; i > 0; i--) result *= a;
    return result;
}

double dinero_acumulado(double dinero, double interes, uint16_t anio){
    double sum = 0;
    for(uint16_t i = 0; i<=anio; i++) sum += my_pow(1+interes/100, i);
    return dinero * sum;
}

double dinero_necesario_anual(double dinero, double interes, uint16_t anio){
    double sum = 0;
    for(uint16_t i = 0; i<=anio; i++) sum += my_pow(1+interes/100, i);
    return dinero / sum;
}


void crear_tabla(double dinero, double interes, uint16_t anio){
    double sum = 0;
    for(uint16_t i = 0; i<=anio; i++){
        sum += my_pow(1+interes/100, i);
        printf("Dinero acumulado en %hu años es %.3f\n", i, dinero * sum);
    }
}


int main(){
    double i = 0; // interest
    uint16_t n = 0; // años
    double A = 0; // dinero
    double f = 0;
    int ej;

    bool flag_ = false;
    while(!flag_){
        printf("Seleccione 1 , 2 o 0 (exit)\r\n");
        scanf("%d", &ej);

        switch(ej){
            case 1:
                printf("Ingrese cantidad de años: \n");
                scanf("%hu",&n);
                printf("Ingrese cantidad de dinero: \n");
                scanf("%lf",&A);
                printf("Ingrese interes: \n");
                scanf("%lf",&i);
                f = dinero_acumulado(A, i, n);
                printf("El dinero acumulado luego de %d años es: %.3f\n", n, f);
                crear_tabla(A, i, n);
                break;
            case 2:
                printf("Ingrese cantidad de años: \n");
                scanf("%hu",&n);
                printf("Ingrese cantidad de dinero: \n");
                scanf("%lf",&f);
                printf("Ingrese interes: \n");
                scanf("%lf",&i);
                A = dinero_necesario_anual(f, i, n);
                printf("El dinero a ahorrar anualmente es: %.3f\n",A);
                crear_tabla(A, i, n);
                break;
            default:
                flag_ = true;
                break;
        }

        }
    return 0;
}
