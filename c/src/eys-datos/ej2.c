/**
 * Módulo Programación en C
 * Entrada y salida de datos
 * Ejercicio 2
 * Autor: Jeremías
 * **/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

void ej_A(){
    char nombre[30];
    printf("Por favor, introfuce tu nombre: ");
    scanf("%s",nombre);
    printf("\nBienvenido %s\n",nombre);
}

void ej_B(){
    float x1;
    float x2;

    printf("Ingrese x1: ");
    scanf("%f", &x1);
    printf("\nIngrese x2: ");
    scanf("%f", &x2);
    printf("\n x1 = %.3f",x1);
    printf("\t x2 = %.3f\n",x2);
}

void ej_C(){
    int64_t a;
    int64_t b;

    printf("Ingrese a: ");
    scanf("%ld", &a);
    printf("\nIngrese b: ");
    scanf("%ld", &b);
    printf("\na + b = %ld \n",a+b);
}

int main(){
    char ej;
    bool flag_ = false;
    while(!flag_){
        printf("Selecione un ejecicio: A, B, C o E (exit): ");
        scanf("%c",&ej);

        switch(ej){
            case 'A':
            case 'a':
                printf("Punto A\n");
                ej_A();
                break;
            case 'B':
            case 'b':
                printf("Punto B\n");
                ej_B();
                break;
            case 'C':
            case 'c':
                printf("Punto C\n");
                ej_C();
                break;
            case 'E':
            case 'e':
                flag_ = true;
                break;
            default:
                break;
        }
    }

    return 0;
}
