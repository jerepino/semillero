/**
 * Módulo Programación en C
 * Entrada y salida de datos
 * Ejercicio 1
 * Autor: Jeremías
 * **/

#include <stdio.h>

void ej_A(){
    int i=103;
    int j=-56;
    float x = 12.687;
    double dx = 0.000000025;
    printf("valor de i = %4d\n",i);
    printf("valor de j = %4d\n",j);
    printf("valor de x = %14.8E\n",x);
    printf("valor de dx = %14.8E\n",dx);

}

void ej_B(){
    int i=103;
    int j=-56;
    float x = 12.687;
    double dx = 0.000000025;
    printf("valor de i = %4d, j = %4d,  x = %14.8E, dx = %14.8E\n", i, j, x, dx);
}

void ej_C(){
    int i=103;
    int j=-56;
    long ix = -158693157400;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de i = %5d \n",i);
    printf("valor de ix = %12ld \n",ix);
    printf("valor de j = %5d \n",j);
    printf("valor de x = %10.5f\n",x);
    printf("valor de u = %5u\n",u);
}

void ej_D(){
    int i=103;
    int j=-56;
    long ix = -158693157400;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de i = %5d, ix = %12ld, j = %5d\n",i, ix, j);
    printf("valor de x = %10.5f, u = %5u\n", x, u);
}

void ej_E(){
    int i=103;
    unsigned u = 35460;
    char c = 'C';

    printf("valor de i = %6d\tu = %6u \tc = %c\n",i, u, c);
}

void ej_F(){
    int j=-56;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de j = %5d\tu = %5u \tx = %11.4f\n",j, u, x);
}

void ej_G(){
    int j=-56;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de j = %-5d\tu = %-5u \tx = %-11.4f\n",j, u, x);
}

void ej_H(){
    int j=-56;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de j = %+5d\tu = %5u \tx = %+11.4f\n",j, u, x);
}

void ej_I(){
    int j=-56;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de j = %05d\tu = %05u \tx = %011.4f\n",j, u, x);
}

void ej_J(){
    int j=-56;
    unsigned u = 35460;
    float x = 12.687;

    printf("valor de j = %05d\tu = %05u \tx = %#11.4f\n",j, u, x);
}

int main(){
    printf("Punto A\n");
    ej_A();
    printf("Punto B\n");
    ej_B();
    printf("Punto C\n");
    ej_C();
    printf("Punto D\n");
    ej_D();
    printf("Punto E\n");
    ej_E();
    printf("Punto F\n");
    ej_F();
    printf("Punto G\n");
    ej_G();
    printf("Punto H\n");
    ej_H();
    printf("Punto I\n");
    ej_I();
    printf("Punto J\n");
    ej_J();
}
