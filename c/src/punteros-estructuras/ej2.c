#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct{
    char nombre[20];
    int victorias;
    int derrotas;
    int empates;
    int goles;
}team;

typedef int (*compareFunction)(const team* a, const team* b);

int bubblesort(team *arr, int n, compareFunction compare){
    int i, j;
    for(i=0; i<n-1; i++){
        for(j=i+1; j<n; j++){
            if(compare(&arr[i], &arr[j])){
                team aux = arr[i];
                arr[i] = arr[j];
                arr[j] = aux;
            }
        }
    }
}

int victorias(const team* a, const team* b){
    return a->victorias < b->victorias;
}

int derrotas(const team* a, const team* b){
    return a->derrotas < b->derrotas;
}

int goles(const team* a, const team* b){
    return a->goles < b->goles;
}

int print_list(team* equipos, int len){
    printf("Lista de equipos:\n");
    for(int i=0; i < len; i++){
        if(i == len-1){
            printf("%d- %s", i, equipos[i].nombre);
        }
        else{
            printf("%d- %s, ", i, equipos[i].nombre);
        }
    }
    printf("\n\n");
}
int main(void){

    team* equipos = (team *) calloc(2, sizeof(team));
    int count = 0;

    bool flag_ = false;
    char ej;

     while(!flag_){
         printf("Selecione:\n");
         printf("A: agregar equipo\n");
         printf("B: ordenar Victorias\n");
         printf("C: ordenar Derrotas\n");
         printf("D: ordenar Goles\n");
         printf("E: Salir\n");
         scanf("%c",&ej);

         switch(ej){
             case 'A':
             case 'a':
                 printf("Ingresando nuevo equipo.\n");

                 printf("Ingrese nombre del equipo.\n");
                 scanf("%s", equipos[count].nombre);

                 printf("Ingrese cantidad de victorias del equipo.\n");
                 scanf("%d",&equipos[count].victorias);

                 printf("Ingrese cantidad de derrotas del equipo.\n");
                 scanf("%d",&equipos[count].derrotas);

                 printf("Ingrese cantidad de empates del equipo.\n");
                 scanf("%d",&equipos[count].empates);

                 printf("Ingrese cantidad de goles del equipo.\n");
                 scanf("%d",&equipos[count].goles);

                 count++;
                 if(count%2 == 0){
                   equipos = (team *) realloc(equipos, sizeof(team)*(count + 2));
                 }

                 break;
             case 'B':
             case 'b':
                 printf("\nOrdenamiento de mayor a menor victorias.\n");
                 bubblesort(equipos, count, victorias);
                 break;
             case 'C':
             case 'c':
                 printf("\nOrdenamiento de mayor a menor derrotas.\n");
                 bubblesort(equipos, count, derrotas);
                 break;
             case 'D':
             case 'd':
                 printf("\nOrdenamiento de mayor a menor goles.\n");
                 bubblesort(equipos, count, goles);
                 break;
             case 'E':
             case 'e':
                 flag_ = true;
                 break;
             default:
                 break;
         }
         print_list(equipos, count);
     }
    free(equipos);
}
