/**
 * Módulo Programación en C
 * Arreglos
 * Ejercicio 1
 * Autor: Jeremías
 * **/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef int (*compareFunction)(float a, float b);

int bubblesort(float *arr, int n, compareFunction compare){
    int i, j;
    for(i=0; i<n-1; i++){
        for(j=i+1; j<n; j++){

            if(compare(arr[i], arr[j])){
                int aux = arr[i];
                arr[i] = arr[j];
                arr[j] = aux;
            }
        }
    }
}

int men2may_abs(float a, float b){
    return abs(a) > abs(b);
}
int men2may(float a, float b){
    return a > b;
}
int may2men_abs(float a, float b){
    return abs(a) < abs(b);
}
int may2men(float a, float b){
    return a < b;
}

void print_array(float *arr, int n){
    printf("[");
    for(int i = 0; i < n; i++) printf("%.2f ",arr[i]);
    printf("]\n");
}

int main(){
   float array [] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
   int len = 10;
   bool flag_ = false;
   char ej;

         printf("Selecione modo de ordenamiento: A, B, C o D. E(exit): ");
     while(!flag_){
         scanf("%c",&ej);

         switch(ej){
             case 'A':
             case 'a':
                 printf("Ordenamiento de menor a mayor en valor absoluto.\n");
                 bubblesort(array, len, men2may_abs);
                 print_array(array, len);
                 break;
             case 'B':
             case 'b':
                 printf("Ordenamiento de menor a mayor algebraicamente (con signo).\n");
                 bubblesort(array, len, men2may);
                 print_array(array, len);
                 break;
             case 'C':
             case 'c':
                 printf("Ordenamiento de mayor a menor en valor absoluto.\n");
                 bubblesort(array, len, may2men_abs);
                 print_array(array, len);
                 break;
             case 'D':
             case 'd':
                 printf("Ordenamiento de mayor a menor algebraicamente (con signo).\n");
                 bubblesort(array, len, may2men);
                 print_array(array, len);
                 break;
             case 'E':
             case 'e':
                 flag_ = true;
                 break;
             default:
                 break;
         }
     }

     return 0;

}
