/**
 * Módulo Programación en C
 * Arreglos
 * Ejercicio 3
 * Autor: Jeremías
 * **/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define NMAX 52
#define NCARTAS 13
#define NPALO 4

typedef struct
{
    int puntos;
    bool gano;
    bool playing;
    int player;
} jugador;

void deck(int *deck_ptr)
{
    int index = 0;
    for (int j = 0; j < NPALO; j++)
    {
        for (int i = 0; i < NCARTAS; i++)
        {
            deck_ptr[index] = i + 1;
            index++;
        }
    }
}

void shuffle_deck(int *deck_ptr)
{
    int aux;
    for (int i = 0; i < NMAX; i++)
    {
        int index = rand() % NMAX;
        aux = deck_ptr[i];
        deck_ptr[i] = deck_ptr[index];
        deck_ptr[index] = aux;
    }
}

void update_player(jugador *player, int carta)
{
    if (carta < 10)
    {
        player->puntos += carta;
    }
    else if (carta > 10 && carta < 13)
    {
        player->puntos += 10;
    }
    else
    {
        if (player->puntos + 11 > 21)
        {
            player->puntos += 1;
        }
        else
        {
            player->puntos += 11;
        }
    }
}

void game(int *deck, jugador *p, int mano)
{
    int opcion;
    if (p->playing)
    {
        printf("Turno jugador %d tu puntaje es %d:  1 (pedir) 2 (plantarse) 0 (exit)\n", p->player, p->puntos);
        scanf("%d", &opcion);

        switch (opcion)
        {
        case 1:
            update_player(p, deck[mano]);
            printf("El puntaje del jugador %d es %d\n", p->player, p->puntos);
            break;
        case 2:
            p->playing = false;
            break;
        default:
            exit(0);
            break;
        }
        if (p->puntos > 21)
        {
            printf("Tu puntaje es %d. Lo siento te pasaste\n", p->puntos);
            p->playing = false;
            p->gano = false;
        }
    }
}

int main()
{
    time_t t;
    srand((unsigned)time(&t));

    int mano = 0;

    jugador player1;
    jugador player2;

    player1.puntos = 0;
    player1.gano = false;
    player1.playing = true;
    player1.player = 1;

    player2.puntos = 0;
    player2.gano = false;
    player2.playing = true;
    player2.player = 2;

    int *deck_ = (int *) calloc(NMAX, sizeof(int));
    deck(deck_);
    shuffle_deck(deck_);

    printf("Blackjack\n");

    while (player1.playing || player2.playing)
    {
        if (mano % 2 == 0)
        {
            game(deck_, &player1, mano);
        }
        else
        {
            game(deck_, &player2, mano);
        }
        mano++;
    }

    if (player1.gano && player2.gano)
    {
        if (player1.puntos < player2.puntos)
        {
            printf("Gano %d\n", player2.player);
        }
        else if (player1.puntos > player2.puntos)
        {
            printf("Gano %d\n", player1.player);
        }
        else{
            printf("Empate\n");
        }

    }
    else
    {
        if (player1.gano)
        {
            printf("Gano %d\n", player1.player);
        }
        else
        {
            printf("Gano %d\n", player2.player);
        }
    }

    free(deck_);

    return 0;
}
